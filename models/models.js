const moment = require('moment');
const uid = require('uid');

exports.race = function(args){
    return {
        raceId: args[0],
        year: args[1],
        round: args[2],
        circuit: args[3],
        name: args[4],
        date: args[5],
        time: args[6],
        url: args[7]
    };
}

exports.constructorResult = function(args){
    return {
        constructorResultId: args[0],
        raceId: args[1],
        constructorId: args[2],
        points: args[3],
        status: args[4]
    }
}

exports.constructorStanding = function(args){
    return {
        constructorStandingId: args[0],
        raceId: args[1],
        constructorId: args[2],
        points: args[3],
        position: args[4],
        positionText: args[5],
        wins: args[6]
    }
}

exports.driver = function(args){
    return {
        driverId: args[0],
        driverRef: args[1],
        number: typeof(args[2]) == 'string' ? null : args[2],
        code: args[3] === "\\N" ? '' : args[3],
        forename: args[4],
        surname: args[5],
        dob: args[6] === "\\N" ? null : moment(args[6]).toISOString(),
        nationality: args[7],
        url: args[8]
    }
}

exports.driverStanding = function(args){
    return {
        driverStandingId: args[0],
        raceId: args[1],
        driverId: args[2],
        points: args[3],
        position: args[4],
        positionText: args[5],
        wins: args[6]
    }
}

exports.lapTime = function(args){
    return {
        lapId: uid(25),
        raceId: args[0],
        driverId: args[1],
        lap: args[2],
        position: args[3],
        time: args[4],
        milliseconds: args[5]
    }
}

exports.pitStop = function(args){
    return {
        raceId: args[0],
        driverId: args[1],
        stop: args[2],
        lap: args[3],
        time: args[4],
        duration: args[5],
        milliseconds: args[6]
    }
}

exports.qualifying = function(args){
    return {
        qualifyId: args[0],
        raceId: args[1],
        driverId: args[2],
        constructorId: args[3],
        number: args[4],
        position: args[5],
        q1: args[6] == "\\N" ? '' : args[6],
        q2: args[7] == "\\N" ? '' : args[7],
        q3: args[8] == "\\N" ? '' : args[8]
    }
}

exports.raceStatus = function(args) {
    return {
        statusId: args[0],
        status: args[1]
    }
}