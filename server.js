const fs = require('fs');
const parse = require('csv-parse');
const gql = require('graphql-tag');
const axios = require('axios');
axios.default.headers = {
    'Content-Type': 'application/json'
}
const moment = require('moment');
const Models = require('./models/models');

const csvPath = 'C:\\Users\\mike\\Downloads\\f1db_csv (1)\\'

const option = process.argv.slice(2)[0];

switch(option){
    case "circuits":
        importCircuits();
        break;
    case "constructors":
        importingConstructors();
        break;
    case "races":
        importRaces();
        break;
    case "constructorresults":
        importConstructorResults();
        break;
    case "constructorstandings":
        importConstructorStandings();
        break;
    case "drivers":
        importDrivers();
        break;
    case "driverstandings":
        importDriverStandings();
        break;
    case "laptimes":
        importLapTimes();
        break;
    case "pitstops":
        importPitStops();
        break;
    case "qualifying":
        importQualifying();
        break;
    case "status":
        importStatuses();
        break;
    default:
        console.log("Please specify an option!");
}

function chunks(inputArray, chunkSize){
    var index = 0;
    var arrayLength = inputArray.length;
    var tempArray = [];

    for(index = 0; index < arrayLength; index += chunkSize){
        let myChunk = inputArray.slice(index, index + chunkSize);
        tempArray.push(myChunk);
    }

    return tempArray
}

function importStatuses(){

}

function importQualifying(){
    const file = "qualifying.csv"
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
            const chunked = chunks(output, 150);
            let toDo = output.length;
            output = null;
            let i = 0;
            
            let done = 0;

            const intr = setInterval(function(){
                chunked[i].forEach(args => {

                    const qualy = Models.qualifying(args);

                    const mutation = `
                        mutation {
                            createQualifying(data: {
                                qualifyId: ${qualy.qualifyId}
                                race: {
                                    connect: {
                                        raceId: ${qualy.raceId}
                                    }
                                }
                                driver: {
                                    connect: {
                                        driverId: ${qualy.driverId}
                                    }
                                }
                                constructor: {
                                    connect: {
                                        constructorId: ${qualy.constructorId}
                                    }
                                }
                                number: ${qualy.number}
                                position: ${qualy.position}
                                q1: "${qualy.q1}"
                                q2: "${qualy.q2}"
                                q3: "${qualy.q3}"
                            }){
                                id
                            }
                        }
                    `

                    axios.post('http://localhost:4466/f1', {query:mutation}).then(res => {
                        console.log(`imported: ${++done}/${toDo}`)
                    })

                })

                if(++i == chunked.length){
                    clearInterval(intr);
                }
            }, 2000)
        })
    })
}

function importPitStops(){
    const file = "pit_stops.csv"
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err, output) => {
            const chunked = chunks(output, 150);
            output = null;
            let i = 0;

            const intr = setInterval(function(){

                chunked[i].forEach(args => {
                    const stop = Models.pitStop(args);

                    const mutation = `
                        mutation {
                            createPitStops(data: {
                                race: {
                                    connect: {
                                        raceId: ${stop.raceId}
                                    }
                                }
                                driver: {
                                    connect: {
                                        driverId: ${stop.driverId}
                                    }
                                }
                                stop: ${stop.stop}
                                lap: ${stop.lap}
                                time: "${stop.time}"
                                duration: "${stop.duration}"
                                milliseconds: ${stop.milliseconds}
                            }){
                                id
                            }
                        }
                    `

                    axios.post('http://localhost:4466/f1', {query:mutation}).then(res => {
                        console.log(res.data);
                    })
                })

                if(++i == chunked.length){
                    clearInterval(intr);
                }
            }, 2000)
            
        })
    })
}

function importLapTimes(){
    const file = "lap_times.csv"
    let test = '';
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
           const chunked = chunks(output, 50);
           const data = null;
           var intr = setInterval(function(){
            console.log(chunked.length);

            chunked[0].forEach(args => {
                const laptime = Models.lapTime(args);
                const mutation = `
                    mutation CreateLap${laptime.lapId} {
                        createLapTime(data: {
                            lapkey: "${laptime.lapId}"
                            race: {
                                connect: {
                                    raceId: ${laptime.raceId}
                                }
                            }
                            driver: {
                                connect: {
                                    driverId: ${laptime.driverId}
                                }
                            }
                            lap: ${laptime.lap}
                            position: ${laptime.position}
                            time: "${laptime.time}"
                            milliseconds: ${laptime.milliseconds}
                        }){
                            id
                        }
                    }
                `

                axios.post('http://localhost:4466/f1', {query: mutation}).then(res => {
                    console.log(res.data);
                })
            })

            if(chunked.length - 1 == 0){
                clearInterval(intr);
            }
            chunked.splice(0,1)
           }, 500)
                
        })
    })
}

function importDriverStandings(){
    const file = "driver_standings.csv"
    console.log('importing driver standings')
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
            const chunked = chunks(output, 150);
            let i = 0;
            

            var intr = setInterval(function(){
                chunked[i].forEach(n => {
                    const standing = Models.driverStanding(n);

                    const mutation = `
                        mutation{
                            createDriverStanding(data: {
                                driverStandingId: ${standing.driverStandingId}
                                race: {
                                    connect: {
                                        raceId: ${standing.raceId}
                                    }
                                }
                                driver: {
                                    connect: {
                                        driverId: ${standing.driverId}
                                    }
                                }
                                points: ${standing.points}
                                position: ${standing.position}
                                positionText: "${standing.positionText}"
                                wins: ${standing.wins}
                            }) {
                                id
                            }
                        }
                    `

                    axios.post(`http://localhost:4466/f1`, {query: mutation}).then(res => {
                        console.log(res.data);
                    })
                })

                if(++i == chunked.length){
                    clearInterval(intr);
                }
            }, 2000)
        })
    })
}

function importDrivers(){
    const file = "driver.csv"
    console.log("importing drivers")
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
            // const chunked = chunks(output, 200)
            output.forEach(args => {
                const driver = Models.driver(args);
                const dob = driver.dob !== null ? `dob: "${driver.dob}"` : ''
                const mutation = `
                mutation {
                    createDriver(data: {
                        driverId: ${driver.driverId}
                        driverRef: "${driver.driverRef}"
                        number: ${driver.number}
                        code: "${driver.code}"
                        forename: "${driver.forename}"
                        surname: "${driver.surname}"
                        ${dob}
                        nationality: "${driver.nationality}"
                        url: "${driver.url}"
                    }) {
                        id
                    }
                }            
            `
               
                axios.post('http://localhost:4466/f1', {
                    query: mutation
                }).then(res => {
                    console.log(res.data);
                })
            })
            
        })
    })
}

function importConstructorStandings(){
    const file = "constructor_standings.csv"
    console.log("importing constructor standings");
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
            let chunked = chunks(output, 200);
            let i = 0;
            
            var intr = setInterval(function(){
                
                chunked[i].forEach(args => {
                    const standing = Models.constructorStanding(args);
                    const mutation = `
                        mutation {
                            createConstructorStanding(data: {
                                standingId: ${standing.constructorStandingId}
                                race: {
                                    connect: {
                                        raceId: ${standing.raceId}
                                    }
                                }
                                constructor: {
                                    connect: {
                                        constructorId: ${standing.constructorId}
                                    }
                                }
                                points: ${standing.points}
                                position: ${standing.position}
                                positionText: "${standing.positionText}"
                                wins: ${standing.wins}
                            }){
                                id
                            }
                        }
                    `

                    axios.post(`http://localhost:4466/f1`, JSON.stringify({query: mutation}), {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(res => {
                        console.log(res.data)
                    })
                })

                if(++i === chunked.length){
                    clearInterval(intr);
                }
            }, 2000)
        })
    })
}

function importConstructorResults(){
    const file = "constructor_results.csv"
    console.log('importing constructor results');
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
           let test = chunks(output, 200);
           let i = 0;
           var intr = setInterval(function(){
               test[i].forEach(args => {
                   const result = Models.constructorResult(args);
                   const mutation = `
                    mutation {
                        createConstructorResult(data: {
                            resultId: ${result.constructorResultId}
                            race: {
                                connect: {
                                    raceId: ${result.raceId}
                                }
                            }
                            constructor: {
                                connect: {
                                    constructorId: ${result.constructorId}
                                }
                            }
                            points: ${result.points}
                        }) {
                            id
                        }
                    }
                    `
                    axios.post(`http://localhost:4466/f1`, JSON.stringify({query: mutation}), {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(res => {
                        console.log(res.data)
                    })

               })
               if(++i == test.length){
                   clearInterval(intr);
               }
               
           }, 5000)
        })
    })
}

function importRaces(){
    const file = "races.csv"
    console.log('importing races');
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {

            output.forEach(args => {
                let race = Models.race(args);
                const mutation = `
                    mutation {
                        createRace(data: {
                            raceId: ${race.raceId},
                            year: ${race.year},
                            round: ${race.round},
                            circuit: {
                                connect: {
                                    circuitId: ${race.circuit}
                                }
                            },
                            name: "${race.name}",
                            date: "${moment(`${race.date} ${race.time}`).toISOString()}",
                            url: "${race.url}"
                        }) {
                            id
                        }
                    }
                `

                axios.post(`http://localhost:4466/f1`, JSON.stringify({query: mutation}), {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => {
                    console.log(res.data)
                })
            })               
        })
    })
}

function importingConstructors(){
    let num = 0;
    const file = 'constructors.csv';
    console.log('importing constructors')
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
            console.log(output[0])
            output.forEach(team => {
                const mutation = `
                    mutation {
                        createConstructor(
                            data: {
                                constructorId: ${team[0]},
                                constructorRef: "${team[1]}",
                                name: "${team[2]}",
                                nationality: "${team[3]}",
                                url: "${team[4].toString()}"
                            }) {
                                id
                            }
                        }
                `
                console.log(mutation);
                axios.post('http://localhost:4466/f1', JSON.stringify({query: mutation}), {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => {
                    console.log(res.data);
                })
            })
        })
    })
}

function importCircuits(){
    const file = 'circuits.csv'
    console.log('importing circuits!');
    fs.readFile(`${csvPath}${file}`, 'utf8', (err,data) => {
        parse(data, {}, (err,output) => {
            output.forEach(circuit => {
                const mutation = `
                    mutation {
                        createCircuit(
                            data: {
                                circuitId: ${circuit[0]},
                                circuitRef: "${circuit[1]}",
                                name: "${circuit[2]}",
                                location: "${circuit[3]}",
                                country: "${circuit[4]}",
                                lat: ${circuit[5]},
                                lng: ${circuit[6]},
                                alt: ${circuit[7]},
                                url: "${circuit[8]}"
                            }
                        ){
                            id
                        }
                    }
                `
    
                axios.post('http://localhost:4466/f1', JSON.stringify({query: mutation}), {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => {
                    console.log(res.data);
                })
            })
            
            
        })
    })
}
